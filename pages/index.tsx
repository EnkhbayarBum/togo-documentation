import Head from "next/head";
import Navbar from "@/components/global/navbar";
import Downloads from "@/components/global/download";

export default function Index() {
  return (
    <>
      <Head>
        <title>Togo app</title>
        <meta name="description" content="Togo app" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      {/* <Navbar /> */}
      <Downloads />
    </>
  );
}
