import BaseApi from './baseApi';

class CheckoutApi extends BaseApi {
  constructor(accessToken) {
    super(accessToken, `/`);
  }
}

export default CheckoutApi;
