import BaseApi from "./baseApi";

class AuthApi extends BaseApi {
  constructor(accessToken) {
    super(accessToken, ``);
  }
}

export default AuthApi;
