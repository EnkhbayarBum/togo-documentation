/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./app/**/*.{js,ts,jsx,tsx}",
    "./pages/**/*.{js,ts,jsx,tsx}",
    "./components/**/*.{js,ts,jsx,tsx}",

    // Or if using `src` directory:
    "./src/**/*.{js,ts,jsx,tsx}",
  ],
  theme: {
    extend: {
      fontFamily: {
        Rubik: ["Rubik", "sans-serif"],
      },
      backgroundImage: (theme) => ({
        "Header-image": "url('assets/Header.png')",
      }),

      backgroundColor: (theme) => ({
        ...theme("colors"),
        text: "#141774",
        bg: "#F4F6F8",
        button: "#505bf1",
        hover: "#4048c0",
        select: "#f19550",
        footer: "#E4E8EB",
        disabled: "#e8f6ff",
      }),
      theme: {
        screens: {
          tablet: "769px",
        },
      },
      borderColor: (theme) => ({
        ...theme("colors"),
        text: "#141774",
        bg: "#F4F6F8",
        button: "#505bf1",
        login: "#3B43B3",
        disabled: "#e8f6ff",
      }),
      width: {
        98: "26rem",
      },
      colors: {
        regalBlue: "#505bf1",
      },
      fill: ["hover", "focus"],
      fill: (theme) => ({
        red: theme("colors.red.500"),
        green: theme("colors.green.500"),
        blue: theme("colors.blue.500"),
      }),
      textColor: {
        text: "#141774",
        link: "#505bf1",
        link_hover: "#3B43B3",
        login: "#505bf1",
        notActive: "#CAD0D7",
        disabled: "#e8f6ff",
      },

      placeholderColor: {
        primary: "#8AB1D5",
      },
      fontSize: {
        large: "40px",
        medium: "24px",
        small: "22px",
        xsmall: "10px",
      },

      screens: {
        xs: "410px",
      },
      animation: {
        bounce: "bounce 2s infinite",
        spin: "spin 12s linear infinite",
        ping: "ping 12s cubic-bezier(1, 1, 1, 1) infinite",
      },
      keyframes: {
        bounce: {
          "0%, 100%": {
            transform: "translateY(-5%)",
            animationTimingFunction: "cubic-bezier(0, 0, 1, 1)",
          },
          "50%": {
            transform: "none",
            animationTimingFunction: "cubic-bezier(0, 0, 0, 0)",
          },
        },
      },
      transitionDelay: {
        0: "0ms",
        2000: "2000ms",
      },
      transformOrigin: {
        "left-right-1/3-3/4": "33% 75%",
      },
    },
  },
  plugins: [],
};
