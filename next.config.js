/** @type {import('next').NextConfig} */
require("next/constants");

module.exports = () => {
  // next.config.js object
  return {
    reactStrictMode: true,
    compress: true,
    env: {
      BASE_API_URL: process.env.NEXT_PUBLIC_API_URL,
      TOKEN_KEY: process.env.NEXT_PUBLIC_TOKEN_KEY,
    },
    images: {
      deviceSizes: [640, 750, 828, 1080, 1200, 1920, 2048, 3840],
      minimumCacheTTL: 60,
      domains: ["https://api.togo.mn"],
    },
    eslint: {
      ignoreDuringBuilds: true,
    },
    swcMinify: false,
    productionBrowserSourceMaps: true,
    future: {
      strictPostcssConfiguration: true,
    },
    async headers() {
      return [
        {
          // This works, and returns appropriate Response headers:
          source: "/:all*(svg|jpg|png|webp)",
          locale: false,
          headers: [
            {
              key: "Cache-Control",
              value:
                "public, max-age=31536000, s-maxage=31536000, stale-while-revalidate=31536000, must-revalidate",
            },
          ],
        },
        {
          // This doesn't work for 'Cache-Control' key (works for others though):
          source: "/_next/image(.*)",
          locale: false,
          headers: [
            {
              key: "Cache-Control",
              value:
                "public, max-age=31536000, s-maxage=31536000, stale-while-revalidate=31536000, must-revalidate",
            },
          ],
        },
      ];
    },
  };
};
