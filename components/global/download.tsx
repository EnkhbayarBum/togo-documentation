import Image from "next/image";
import Aos from "aos";
import "aos/dist/aos.css";
import { useEffect } from "react";

export default function Downloads() {
  useEffect(() => {
    Aos.init({ duration: 1000 });
  }, []);
  return (
    // <div>
    //   <div className="w-full p-4 pt-8 lg:px-8 max-w-xl md:max-w-5xl mx-auto">
    //         <div className="">
    //           <Image
    //             src="/assets/documentation/icons/logo1.svg"
    //             alt={"logo"}
    //             width={"140"}
    //             height={"100"}
    //           />
    //     </div>
    //     </div>
    // <div className="md:h-screen w-full bg-gradient-to-b from-[#101233] to-[#2d3388] flex items-center">
    //   <div className="max-w-xl md:max-w-5xl mx-auto">
    //     <div className="grid md:grid-cols-2 items-center pt-80 p-4 md:pt-0">
    //       <div data-aos="zoom-left" className="w-full md:flex justify-center rounded-2xl hidden">
    //         <img src="/assets/documentation/home.png" alt="" className="rounded-2xl" />
    //       </div>
    //       <div className="space-y-10 sm:space-y-12">
    //         <h1
    //           data-aos="zoom-up"
    //           className="text-white text-4xl lg:text-6xl font-bold flex items-center"
    //         >
    //           Аялахад хялбар боллоо
    //         </h1>
    //         <p
    //           data-aos="zoom-up"
    //           className="text-white/80 font-normal text-xl"
    //         >
    //           Таны аялалын явцад тохиолдож болох бүхий л болзошгүй эрсдэл
    //         </p>
    //         <div data-aos="zoom-up">
    //           <h1 className="font-bold text-xl mb-4 text-white text-center md:text-start">
    //             Аппликейшн татах
    //           </h1>
    //           <div className="flex justify-center md:justify-start items-center flex-wrap gap-4">
    //             <a
    //               href="https://play.google.com/store/apps/details?id=mn.mic.togo"
    //               target={"_blank"}
    //             >
    //               <Image
    //                 src="/assets/documentation/icons/apple.png"
    //                 alt="star"
    //                 className="opacity-80 hover:opacity-100"
    //                 width={"184"}
    //                 height={"53"}
    //               />
    //             </a>
    //             <a
    //               href="https://play.google.com/store/apps/details?id=mn.mic.togo"
    //               target={"_blank"}
    //             >
    //               <Image
    //                 src="/assets/documentation/icons/android.png"
    //                 alt="star"
    //                 className="opacity-80 hover:opacity-100"
    //                 width={"184"}
    //                 height={"53"}
    //               />
    //             </a>
    //           </div>
    //         </div>
    //       </div>
    //       <div data-aos="zoom-left" className="w-full md:hidden justify-center rounded-2xl flex pt-8">
    //         <img src="/assets/documentation/home.png" alt="" className="rounded-2xl" />
    //       </div>
    //     </div>
    //   </div>
    // </div>
    // </div>
    <div className="md:h-screen bg-text bg-gradient-to-b from-[#101233] to-[#2d3388]">
      <div className="max-w-xl md:max-w-5xl mx-auto p-8">
        <div className="">
            <Image
                src="/assets/documentation/icons/logo1.svg"
                alt={"logo"}
                width={"140"}
                height={"100"}
              />
        </div>
        <div className="mt-0 md:mt-40">
          <div className="w-full grid md:grid-cols-5 items-center pt-20 md:pt-0">
            <div data-aos="zoom-left" className="w-full md:col-span-2 md:flex md:justify-start justify-center hidden">
              <img src="/assets/documentation/home.png" alt=""/>
            </div>
            <div className="space-y-10 sm:space-y-12 md:col-span-3">
           <h1
              data-aos="zoom-up"
              className="text-white text-4xl lg:text-6xl font-bold flex items-center"
            >
              Аялахад хялбар боллоо
            </h1>
            <p
              data-aos="zoom-up"
              className="text-white/80 font-normal text-xl"
            >
              Таны аяллын явцад тохиолдож болох бүхий л болзошгүй эрсдэлийг анхааруулж, шаардлагатай үйлчилгээ үзүүлж таны үнэнч нөхөр байна
            </p>
            <div data-aos="zoom-up">
              <h1 className="font-bold text-xl mb-4 text-white text-center sm:text-start">
                Аппликейшн татах
              </h1>
              <div className="flex justify-center sm:justify-start items-center flex-wrap gap-4">
                <a
                  href="https://apps.apple.com/mn/app/togo-mn/id6444095550"
                  target={"_blank"}
                >
                  <Image
                    src="/assets/documentation/icons/apple.png"
                    alt="star"
                    className="opacity-80 hover:opacity-100"
                    width={"184"}
                    height={"53"}
                  />
                </a>
                <a
                  href="https://play.google.com/store/apps/details?id=mn.mic.togo"
                  target={"_blank"}
                >
                  <Image
                    src="/assets/documentation/icons/android.png"
                    alt="star"
                    className="opacity-80 hover:opacity-100"
                    width={"184"}
                    height={"53"}
                  />
                </a>
              </div>
            </div>
          </div>
          </div>
        </div>
        <div data-aos="zoom-left" className="w-full md:hidden justify-center rounded-2xl flex pt-8">
         <img src="/assets/documentation/home.png" alt="" className="rounded-2xl" />
       </div>
      </div>
    </div>
  );
}
