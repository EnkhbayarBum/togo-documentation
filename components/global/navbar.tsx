import Image from "next/image";

export default function Navbar() {

  return (
    <>
       
          <div className="w-full p-4 pt-8 lg:px-8 max-w-xl md:max-w-5xl mx-auto">
            <div className="">
              <Image
                src="/assets/documentation/icons/logo1.svg"
                alt={"logo"}
                width={"140"}
                height={"100"}
              />
        </div>
      </div>
     
    </>
  );
}
0;
